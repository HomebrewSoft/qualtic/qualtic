# -*- coding: utf-8 -*-
{
    'name': 'Qualtic',
    'version': '13.0.1.1.0',
    'author': 'HomebrewSoft',
    'website': 'https://homebrewsoft.dev/',
    'license': 'LGPL-3',
    'depends': [
    ],
    'data': [
        # security
        # data
        # reports
        # views
        'views/res_partner.xml',
    ],
}
