# -*- coding: utf-8 -*-
from odoo import _, api, fields, models


class Partner(models.Model):
    _inherit = 'res.partner'

    image_rights_consent = fields.Boolean(
    )
    newsletter_consent = fields.Boolean(
    )
